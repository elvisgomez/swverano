/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;


public interface Validar {
    public int validar(Empleado e);
    public boolean registrar(Empleado e);
    public String mostrar();
    public List listar();
    public Empleado list(int id);
    public boolean add(Empleado per);
    public boolean edit(Empleado per);
    public boolean eliminar(int id);
    
   
    
}
