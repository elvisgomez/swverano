/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author raul
 */
public class EmpleadoDAO implements Validar{
   
    Connection con;
    Conexion cn=new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    int res=0;
    Empleado p;
   
    @Override
    public int validar(Empleado em) {
        int res=0;
        String sql="Select* From empleado Where usuario=? and contraseña=?";
        try{
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.setString(1, em.getUsuario());
            ps.setString(2, em.getContraseña());
            rs=ps.executeQuery();  
            while(rs.next()){
                res=res+1;
                em.setUsuario(rs.getString("usuario"));
                em.setContraseña(rs.getString("contraseña"));
            }
            if(res==1){
                return 1;
            }else{
                return 0;
            }
        }catch(Exception e){
            return 0;
        }
    }
    
    
    @Override
    public boolean registrar(Empleado em) {
       
        
        try{con=cn.getConnection();
           
            ps = con.prepareStatement("insert into empleado (nombres,apellidos,ci_empleado,telefono_empleado,correo_empleado,usuario,contraseña) VALUE (?,?,?,?,?,?,?)");
         
            ps.setString(1, em.getNombre());
            ps.setString(2, em.getApellidos());
            ps.setInt(3,em.getCi());
            ps.setInt(4, em.getTelefono());
            ps.setString(5, em.getCorreo());
            ps.setString(6, em.getUsuario());
            ps.setString(7, em.getContraseña());
            
            int respuesta = ps.executeUpdate();
            ps.close();
            return respuesta > 0;
        } catch (SQLException e) {
            System.out.println("error");
            return false;
        }
            
       }

    @Override
    public String mostrar() {
    
        try{ con=cn.getConnection();  
            Statement smt;
              smt = con.createStatement();
            rs = smt.executeQuery("select * from empleado"); 
            while (rs.next()) {
            return rs.getString("usuario");
      
            }
        } catch (SQLException e) {
            System.out.println("error");
          
        }
       return null;
    }
    @Override
      public List listar() {
        ArrayList<Empleado>list=new ArrayList<>();
        String sql="select * from empleado";
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){
                Empleado per=new Empleado();
                per.setUsuario(rs.getString("usuario"));
                per.setApellidos(rs.getString("apellidos"));
                per.setCi(rs.getInt("ci_empleado"));
                per.setTelefono(rs.getInt("telefono_empleado"));
                per.setCorreo(rs.getString("correo_empleado"));
                per.setUsuario(rs.getString("usuario"));
                per.setContraseña(rs.getString("contraseña"));
                list.add(per);
            }
        } catch (Exception e) {
        }
        return list;
    }

    @Override
    public Empleado list(int ci) {
        String sql="select * from Empleado where ci_empleado="+ci;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){                
              
                p.setCi(rs.getInt("ci_empleado"));
                p.setNombre(rs.getString("nombres"));
                p.setApellidos(rs.getString("apellidos"));
               
             
                
            }
        } catch (Exception e) {
        }
        return p;
    }

    public boolean add(Empleado per) {
       String sql="insert into persona(DNI, Nombres)values('"+per.getCi()+"','"+per.getNombre()+"')";
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
       return false;
    }

    public boolean edit(Empleado per) {
        String sql="update empleado set usuario='"+per.getUsuario()+"',contraseña='"+per.getContraseña()+"'where ci_empleado="+per.getCi();
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean eliminar(int id) {
        String sql="delete from empleado where Id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
 }
 
