/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Empleado;
import Modelo.EmpleadoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Controlador extends HttpServlet {

    EmpleadoDAO edao=new EmpleadoDAO();
    Empleado emp=new Empleado();
    int res;
    
    boolean respuesta=false;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
        
        String accion=request.getParameter("accion");
       if(accion.equals("Ingresar")){
           String user=request.getParameter("txtuser");
           String pasword=request.getParameter("txtpass");
           emp.setUsuario(user);
           emp.setContraseña(pasword);
           res=edao.validar(emp);
           if(user.equals("root") && pasword.equals("administrador"))
           {
               request.getRequestDispatcher("Formulario.jsp").forward(request, response);
             
           }
           if(res==1){
               request.getRequestDispatcher("Chat.jsp").forward(request, response);
           }else{
               
               request.getRequestDispatcher("index.jsp").forward(request, response);
           }
       }
       if(accion.equals("Registrar")){
           String nombre=request.getParameter("txtnombres");
           String apellidos=request.getParameter("txtapellidos");
           String ci=request.getParameter("txtci");
           String telefono=request.getParameter("txttelf");
           String correo=request.getParameter("txtcorreo");
           if(nombre.equals("") || apellidos.equals("") ||ci.equals("") || telefono.equals("") ||correo.equals("")){
               respuesta=false;
               System.out.print("llenar todos los campos");
           }else{    
               String [] a=nombre.split(" ");
               String [] b=apellidos.split(" ");
               String s=a[0]+" "+b[0];
            emp.setNombre(nombre);
            emp.setCi(Integer.parseInt(ci));
            emp.setTelefono(Integer.parseInt(telefono));
            emp.setCorreo(correo);
            emp.setApellidos(apellidos);            
            emp.setUsuario(s);
            emp.setContraseña(ci);
            
           }
           respuesta=edao.registrar(emp);
           if(respuesta==true){
               System.out.print("guardado");
              // response.sendRedirect("index.jsp");
               response.sendRedirect("vistas/listar.jsp");
             
           }else{response.sendRedirect("formulario.jsp");}
       }
       if(accion.equals("Cancelar")){}
       
       
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
