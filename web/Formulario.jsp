        
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="Modelo.Empleado"%>
<%@page import="Modelo.EmpleadoDAO"%>


<%@page import="java.awt.TrayIcon.MessageType"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Formulario de Registro</title>
    </head>
    <body>
        <div class="d-flex">
            <div class="card mt-2 col-lg-4">
                <div class="card-body">
                    <form  class=table-responsive action="Controlador">
                        <div class="form-group text-center">
                            <h2>Registrar Usuario</h2>
                            <img src="imagenes/logo.png" heigth="30" width="90"/>
                            <label>Chatea</label>
                        </div>
                        <div class="form-group" >
                            <label>Nombre (s)</label>
                            <input type="text" name="txtnombres" class="form-control" placeholder="Ingrese Nombre(s)"
                                   pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð '-]{2,30}" title="*maximo 20 carcateres, Solo ingresar Letras"  required>
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" name="txtapellidos" class="form-control" placeholder="Ingrese Apellidos"
                                   pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð '-]{2,30}" title="*max:30 min:2 carcateres"  required>
                        </div>
                        <div class="form-group">
                            <label>Cedula de Identitad</label>
                            <input type="text" name="txtci" class="form-control" placeholder="Ingrese Cedula de Identidad"
                                   pattern="^([0-9]+[a-zA-z]{0,2}){5,12}$"  title="*min:5 max:10caracteres, ej:55555555 55555555cb" required>
                        </div>
                        <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" name="txttelf" class="form-control" placeholder="Ingrese Telefono"
                                   pattern="[0-9]{1,8}" title="*Ingrese Solo Numeros, Max 8 caracteres" required>
                        </div>
                        <div class="form-group">
                            <label>Correo</label>
                                <input type="text" name="txtcorreo" class="form-control" placeholder="Ingrese Correo"
                                   pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" title="ejemplo@ejemplo.ejemplo" required>
                        </div>
                        
                            <input type="submit" name="accion" value="Registrar" class="btn btn-primary btn-block">
                            <input type="reset" name="accion" value="Cancelar" class="btn btn-primary btn-block">
                        
                            <div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>¡Cuidado!</strong> Es muy importante que leas este mensaje de alerta.
</div>
                    </form>
                </div>
            </div>
            <div class="container">
          
            <a class="btn btn-success" href="Crud?accion=listar">Lista de personas</a>
            <a class="btn btn-success" href="Formulario.jsp">Registrar</a>
            <br>
            <br>
            <table class=" table-responsive" >
                <thead>
                    <tr>
                        <th class="text-center">ID E.</th>
                        <th class="text-center">Nombres</th>
                        <th class="text-center">Apeliidos</th>
                        <th class="text-center">Carnet </th>
                        <th class="text-center">telefono</th>
                        <th class="text-center">correo</th>
                         <th class="text-center">usuario</th>
                          <th class="text-center">contrasenia</th>
                          
                        <th class="text-center">ACCIONES</th>
                    </tr>
                </thead>
                <%
                    EmpleadoDAO dao=new EmpleadoDAO();
                    List<Empleado>list=dao.listar();
                    Iterator< Empleado>iter=list.iterator();
                    Empleado per=null;
                    while(iter.hasNext()){
                        per=iter.next();
                    
                %>
                <tbody>
                    <tr>
                        
                        <td class="text-center"><%= per.getNombre()%></td>
                        <td class="text center" > <%= per.getApellidos()%></td>
                        <td class="text center" > <%= per.getCi()%></td>
                        <td class="text center" > <%= per.getTelefono()%></td>
                        <td class="text center" > <%= per.getCorreo()%></td>
                        <td class="text center" > <%= per.getUsuario()%></td>
                        <td class="text center" > <%= per.getCorreo()%></td>
                        <td class="text center" > <%= per.getApellidos()%></td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-xs" href="Crud?accion=editar&ci=<%= per.getCi()%>">Editar</a>
                            <a class="btn btn-primary btn-xs" href="Crud?accion=eliminar&ci=<%= per.getCi()%>">Remove</a>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>

        </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>


</html>