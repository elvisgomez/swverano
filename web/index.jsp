<%-- 
    Document   : index
    Created on : 10-02-2020, 02:15:55 PM
    Author     : egm
--%>

<%@page import="Controlador.Controlador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Login</title>
        
       
    </head>
     <%
            //Al hacer click en el botón ingresar
            if (request.getParameter("accion") != null) 
            {
                //Crea dos strings, una para el user y otra para el password.
                String username = request.getParameter("txtuser");
                String password = request.getParameter("txtpass");
                
                //Si ambas son "admin"...
                if (username.equals(" ")&&password.equals(" "))
                {
                    //Redirecciona al servlet 'crear_departamento.do'
                    response.sendRedirect("crear_departamento.do");
                }
                //Si no...
                else
                {
                    //Muestra un mensaje javascript señalando que hay daros erróneos
                    out.println("<script>alert('Usuario o contraseña incorrecta');</script>");
                }
            }
        %>
    <body>
        <div class="container mt-4 col-lg-4">
            <div class="card col-sm-10">
                <div class="card-body">
                    <form action="Controlador" name="formulario">
                        <div class="form-group text-center">
                            <img src="imagenes/logo.png" heigth="70" width="170"/>
                            <label>Chatea</label>
                            <h1>Bienvenido</h1>
                        </div>
                        <div class="form-group">
                            <label>Usuario</label>
                            <input type="text" id ="txtuser" name="txtuser" class="form-control" placeholder="Ingrese Usuario"
                                    pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,30}" title="'ej=Usuario' *maximo 20 carcateres"  required>
                            <label id ="error" style="color:blue" ></label>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" id="txtpass" name="txtpass" class="form-control" placeholder="Ingrese Contraseña" 
                                  pattern="[a-zA-Z0-9]{6,20}" title="minimo 6 caracteres"  required>
                            <label id ="error1" style="color:blue" ></label>
                        </div>
                        <input type="submit"  onclick="return validar();"  name="accion" value="Ingresar" class="btn btn-primary btn-block" ><br>
                        <%
                            Controlador con =new Controlador();
                           
                        
                       %>
                        <!--<div class="alert alert-danger ">
                            <button class ="close" data-dismiss="alert"></button><span>&times;</span> 
                            alerta
                        </div>-->
                        <label>¿Se te olvido la contraseña?</label>
                    </form>
                </div>
            </div>
        </div>
    
</body>
</html>
