-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2020 at 10:30 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_mensajeria`
--

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `id_empleado` int(100) NOT NULL,
  `nombres` varchar(60) DEFAULT NULL,
  `apellidos` varchar(60) DEFAULT NULL,
  `ci_empleado` int(8) DEFAULT NULL,
  `telefono_empleado` int(8) DEFAULT NULL,
  `correo_empleado` varchar(50) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `contraseña` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`id_empleado`, `nombres`, `apellidos`, `ci_empleado`, `telefono_empleado`, `correo_empleado`, `usuario`, `contraseña`) VALUES
(1, 'Raul', 'Zurita Teran', 7960242, 63949121, 'raulzurita91@gmail.com', 'root', 'admin'),
(2, 'Elvis', 'Gomez Marca', 7901234, 70000001, 'elvisgomez@gmail.com', 'Elvis Gomez', '7901234'),
(3, 'Erwin Orlando', 'Manzaneda', 7901235, 70000010, 'erwinmanzaneda@gmail.com', 'Erwin Manzaneda', '7901235'),
(4, 'Richar', 'Irazabal Belles', 7901236, 70000020, 'richarirasabal@gmail.com', 'Richar Irasabal', '7901236'),
(5, 'Raul', 'Zurita Teran', 7960242, 63949121, 'raulzurita91@gmail.com', 'Raul Zurita', '7960242');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_empleado`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_empleado` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
